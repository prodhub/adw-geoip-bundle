<?php

namespace ADW\GeoIpBundle\Tests\Functional;

use ADW\GeoIpBundle\Model\GeoIpHandler;
use ADW\GeoIpBundle\Model\Location;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GeoIpHandlerTest extends WebTestCase
{
    /**
     * @var GeoIpHandler
     */
    private $handler;

    protected function setUp()
    {
        require_once __DIR__ . '/../Fixtures/TestKernel.php';

        $kernel = new \AppKernel('test', true);
        $kernel->boot();
        $container = $kernel->getContainer();
        $this->handler = $container->get('adw.geoip.handler');
    }

    public function testGetCity()
    {
        $this->assertInternalType('string', $this->handler->getCity('79.142.82.62'));
    }

    public function testGetCountry()
    {
        $this->assertInternalType('string', $this->handler->getCountry('79.142.82.62'));
    }

    public function testResultInstance()
    {
        $this->assertNull($this->handler->getLocation('127.0.0.1'));
        $this->assertInstanceOf(Location::class, $this->handler->getLocation('79.142.82.62'));

        foreach ($this->getRandomIps() as $ip) {
            $location = $this->handler->getLocation($ip);
            $this->assertTrue((bool)( $location instanceof Location | $location === null ));
        }
    }

    public function testGetArrayFromResult()
    {
        foreach ($this->getRandomIps() as $ip) {
            $location = $this->handler->getLocation($ip);
            if ($location) {
                $this->assertInternalType('array', $location->toArray());
            }
        }
    }

    public function testGetJsonFromResult()
    {
        foreach ($this->getRandomIps() as $ip) {
            $location = $this->handler->getLocation($ip);
            if ($location) {
                $this->assertJson($location->toJson());
            }
        }
    }

    public function getRandomIp() {
        $ip = [];
        while (count($ip) < 4) {
            $ip[] = rand(0, 255);
        }
        return join('.', $ip);
    }

    public function getRandomIps($count=1000) {
        $ips = array();
        while (count($ips) < $count) {
            $ips[] = $this->getRandomIp();
        }
        return $ips;
    }
}