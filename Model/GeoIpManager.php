<?php

namespace ADW\GeoIpBundle\Model;

use ADW\GeoIpBundle\LocationProvider\LocationProviderInterface;


/**
 * Class GeoIpManager
 * @package ADW\GeoIpBundle\Model
 */
class GeoIpManager
{
    /**
     * @var array
     */
    private $providers = [];

    /**
     * @param LocationProviderInterface $provider
     * @return GeoIpManager
     */
    public function registerProvider(LocationProviderInterface $provider)
    {
        $this->providers[] = $provider;

        return $this;
    }

    /**
     * @param string $ip
     * @return Location|null
     */
    public function getLocationByIp($ip)
    {
        foreach ($this->providers as $provider) {
            if ($provider->hasIp($ip)) {
                return $provider->findLocationByIp($ip);
            }
        }

        return null;
    }
}