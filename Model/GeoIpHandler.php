<?php

namespace ADW\GeoIpBundle\Model;


use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class GeoIpHandler
 * @package ADW\GeoIpBundle\Model
 */
class GeoIpHandler
{
    /**
     * @var GeoIpManager
     */
    private $geoIpManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * GeoIpHandler constructor.
     * @param RequestStack $requestStack
     * @param GeoIpManager $geoIpManager
     */
    public function __construct(RequestStack $requestStack, GeoIpManager $geoIpManager)
    {
        $this->requestStack = $requestStack;
        $this->geoIpManager = $geoIpManager;
    }

    /**
     * @param string $ip
     * @return Location|false
     */
    public function getLocation($ip = null)
    {
        if (!$ip) {
            $ip = $this->getCurrentRequest()->getClientIp();
        }

        return $this->geoIpManager->getLocationByIp($ip);
    }

    /**
     * @param string $ip
     * @return string|null
     */
    public function getCountry($ip = null)
    {
        $location = $this->getLocation($ip);

        if (!$location || $location->getType() !== Location::TYPE_COUNTRY) {
            return null;
        }

        return $location->getName();
    }

    /**
     * @param string $ip
     * @return string|null
     */
    public function getCity($ip = null)
    {
        $location = $this->getLocation($ip);

        if (!$location) {
            return null;
        }

        return $this->getCityByLocation($location);
    }

    /**
     * @param Location $location
     * @return string|null
     */
    private function getCityByLocation(Location $location)
    {
        if ($location->getType() === Location::TYPE_CITY) {
            return $location->getName();
        }

        if ($location->hasChild()) {
            return $this->getCityByLocation($location->getChild());
        }

        return null;
    }

    /**
     * @return null|\Symfony\Component\HttpFoundation\Request
     */
    private function getCurrentRequest()
    {
        return $this->getRequestStack()->getCurrentRequest();
    }

    /**
     * @return RequestStack
     */
    private function getRequestStack()
    {
        return $this->requestStack;
    }
}