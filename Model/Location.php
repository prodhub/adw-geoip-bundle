<?php

namespace ADW\GeoIpBundle\Model;


/**
 * Class Location
 * @package ADW\GeoIpBundle\Model
 */
class Location
{
    const TYPE_COUNTRY  = 'country';
    const TYPE_DISTRICT = 'district';
    const TYPE_REGION   = 'region';
    const TYPE_CITY     = 'city';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var Location
     */
    private $child;

    /**
     * @var array
     */
    private $coordinates = [];

    /**
     * @var string
     */
    private $type;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Location
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Location
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @param Location $child
     * @return Location
     */
    public function setChild($child)
    {
        $this->child = $child;

        return $this;
    }

    /**
     * @return array
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param array $coordinates
     * @return Location
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasChild()
    {
        return $this->child ? true : false;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Location
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getName();
        if ($this->hasChild()) {
            $result .= ' '. $this->getChild()->__toString();
        }

        return $result;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [
            $this->getType() => [
                'name' => $this->getName(),
                'code' => $this->getCode(),
                'coordinates' => $this->getCoordinates()
            ]
        ];

        if ($this->hasChild()) {
            return array_merge($array, $this->getChild()->toArray());
        }

        return $array;
    }

    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}