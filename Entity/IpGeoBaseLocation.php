<?php

namespace ADW\GeoIpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class IpGeoBaseLocation
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="adw_geoip_ipgeobase_location" ,indexes={@ORM\Index(name="search_geoip_range", columns={"ip_min", "ip_max"})})
 *
 */
class IpGeoBaseLocation
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $countryFullname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $district;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;


    /**
     * @var string
     *
     * @ORM\Column(name="ip_min", type="bigint", nullable=false)
     */
    private $ipMin;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_max", type="bigint", nullable=false)
     */
    private $ipMax;

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getIpMin()
    {
        return $this->ipMin;
    }

    /**
     * @param mixed $ipMin
     */
    public function setIpMin($ipMin)
    {
        $this->ipMin = $ipMin;
    }

    /**
     * @return mixed
     */
    public function getIpMax()
    {
        return $this->ipMax;
    }

    /**
     * @param mixed $ipMax
     */
    public function setIpMax($ipMax)
    {
        $this->ipMax = $ipMax;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCountryFullname()
    {
        return $this->countryFullname;
    }

    /**
     * @param string $countryFullname
     * @return IpGeoBaseLocation
     */
    public function setCountryFullname($countryFullname)
    {
        $this->countryFullname = $countryFullname;

        return $this;
    }

}