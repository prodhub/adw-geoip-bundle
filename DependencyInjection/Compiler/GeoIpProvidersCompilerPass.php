<?php

namespace ADW\GeoIpBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class GeoIpProvidersCompiler
 * @package AppBundle\DependencyInjection\Compiler
 */
class GeoIpProvidersCompilerPass implements CompilerPassInterface
{
    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('adw.geoip.manager')) {
            return;
        }

        $definition = $container->getDefinition(
            'adw.geoip.manager'
        );

        $providers = $container->findTaggedServiceIds('adw.geoip.provider');

        foreach ($providers as $id => $attributes) {
            $definition->addMethodCall('registerProvider', array(new Reference($id)));
        }
    }
}
