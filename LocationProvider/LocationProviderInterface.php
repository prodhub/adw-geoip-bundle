<?php

namespace ADW\GeoIpBundle\LocationProvider;

use ADW\GeoIpBundle\Model\Location;


/**
 * Interface LocationProviderInterface
 * @package ADW\GeoIpBundle\Model
 */
interface LocationProviderInterface
{
    /**
     * @param string $ip
     * @return mixed
     */
    public function hasIp($ip);

    /**
     * @param string $ip
     * @return Location
     */
    public function findLocationByIp($ip);
}