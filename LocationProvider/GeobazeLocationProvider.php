<?php

namespace ADW\GeoIpBundle\LocationProvider;

use ADW\GeoIpBundle\Model\Location;


/**
 * Class GeobazeLocationProvider
 * @package ADW\GeoIpBundle\Model
 */
class GeobazeLocationProvider implements LocationProviderInterface
{
    /**
     * @var \GeobazaQuery
     */
    private $query;

    /**
    private $query;
     * @var string
     */
    private $locale;

    /**
     * GeobazeLocationProvider constructor.
     * @param string $locale
     */
    public function __construct($locale)
    {
        $this->query = new \GeobazaQuery();
        $this->locale = $locale;
    }

    /**
     * @param string $ip
     * @return bool
     */
    public function hasIp($ip)
    {
        try {
            if ($this->query->get_path($ip)->is_special) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $ip
     * @return Location|null
     */
    public function findLocationByIp($ip)
    {
        $response = $this->findResponseByIp($ip);
        $location = $this->parseResponse($response);

        return $location;
    }

    /**
     * @param string $ip
     * @return mixed
     */
    private function findResponseByIp($ip)
    {
        return $this->query->get_path($ip);
    }

    /**
     * @param $response
     * @return Location|null
     */
    private function parseResponse($response)
    {
        if (!$response->country) {
            return null;
        }

        return $this->parseResponsePart($response->country);
    }

    /**
     * @param $geoEntity
     * @return string
     */
    private function parseNameByLocale($geoEntity)
    {
        if (isset($geoEntity->{$this->locale})) {
            return $geoEntity->{$this->locale};
        }

        return $geoEntity->en;
    }

    /**
     * @param mixed $responsePart
     * @return Location
     */
    private function parseResponsePart($responsePart)
    {
        $location = new Location();
        $name = $this->parseNameByLocale($responsePart->translations[0]);
        $location->setName($name);
        $location->setCode($responsePart->iso_id);
        $location->setType($this->getLocationType($responsePart));
        $coordinates = [
            'latitude' => $responsePart->geography->center->latitude,
            'longitude' => $responsePart->geography->center->longitude,
        ];
        $location->setCoordinates($coordinates);
        if (isset($responsePart->child)) {
            $location->setChild($this->parseResponsePart($responsePart->child));
        }

        return $location;
    }

    /**
     * @param mixed $responsePart
     * @return string
     */
    public function getLocationType($responsePart)
    {
        switch ($responsePart->type) {
            case Location::TYPE_COUNTRY:
                return Location::TYPE_COUNTRY;
            case Location::TYPE_REGION:
                if ($responsePart->level == 2) {
                    return Location::TYPE_DISTRICT;
                }
                return Location::TYPE_REGION;
            default:
                return Location::TYPE_CITY;
        }
    }
}