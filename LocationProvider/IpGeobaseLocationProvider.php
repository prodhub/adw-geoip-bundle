<?php

namespace ADW\GeoIpBundle\LocationProvider;


use ADW\GeoIpBundle\Entity\IpGeoBaseLocation;
use ADW\GeoIpBundle\Model\Location;
use Doctrine\ORM\EntityManager;

class IpGeobaseLocationProvider implements LocationProviderInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * IpGeobaseLocationProvider constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $ip
     * @return bool|mixed
     */
    public function hasIp($ip)
    {
        $qb = $this->getRepository()->createQueryBuilder('i');
        $location = $qb->where(':ip BETWEEN i.ipMin AND i.ipMax')
            ->setParameter('ip', ip2long($ip))
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if ($location) {
            return $location;
        }

        return false;
    }

    /**
     * @param string $ip
     * @return Location|null
     */
    public function findLocationByIp($ip)
    {
        if ($location = $this->hasIp($ip)) {
            return $this->parseResponse($location);
        }

        return null;
    }

    /**
     * @param IpGeoBaseLocation $response
     * @return Location|null
     */
    private function parseResponse(IpGeoBaseLocation $response)
    {
        $country = $this->parseResponsePart($response->getCountry(), $response->getCode(), Location::TYPE_COUNTRY);

        if (!$country) {
            return null;
        }

        $region = $this->parseResponsePart($response->getRegion(), '', Location::TYPE_REGION);
        $district = $this->parseResponsePart($response->getDistrict(), '', Location::TYPE_DISTRICT);
        $city = $this->parseResponsePart(
            $response->getCity(),
            '',
            Location::TYPE_CITY,
            [
                'latitude' => $response->getLatitude(),
                'longitude' => $response->getLongitude(),
            ]
        );

        if ($city)
            $district->setChild($city);
        if ($district)
            $region->setChild($district);
        if ($region)
            $country->setChild($region);

        return $country;
    }

    /**
     * @param string $name
     * @param string $code
     * @param string $type
     * @param array $coordinates
     * @return Location|null
     */
    private function parseResponsePart($name, $code = '', $type = '', $coordinates = [])
    {
        if (!$name) {
            return null;
        }

        $location = new Location();
        $location->setName($name);
        $location->setCode($code);
        $location->setType($type);
        $location->setCoordinates($coordinates);

        return $location;
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('ADWGeoIpBundle:IpGeoBaseLocation');
    }
}