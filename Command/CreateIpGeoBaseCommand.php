<?php

namespace ADW\GeoIpBundle\Command;

use ADW\GeoIpBundle\Entity\IpGeoBaseLocation;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;


class CreateIpGeoBaseCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName("adw:ipgeobase:create")
            ->setDescription("Create geo ip base")
            ->setHelp("Create ip geobase from file")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return boolean
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $kernel = $this->getContainer()->get('kernel');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $countriesFile = $kernel->locateResource('@ADWGeoIpBundle/Bases/IpGeobase/cidr_optim.txt');
        if (file_exists($countriesFile)) {
            $progress = new ProgressBar($output, count(file($countriesFile)));
            $progress->setFormatDefinition('custom', ' [%bar%] %elapsed%/%estimated% %current% из %max% %message%');
            $progress->setFormat('custom');
            $progress->setMessage('записей загруженно');
            if ($countriesHandler = fopen($countriesFile, 'r')) {
                $i = 0;
                $em->createQuery('DELETE FROM ADWGeoIpBundle:IpGeoBaseLocation')->execute();

                while (($row = fgetcsv($countriesHandler, null, "\t")) !== false) {
                    $location = new IpGeoBaseLocation();
                    $location->setCode($row[3]);
                    $country = $this->getCountryByIsoCode($row[3]);
                    $location->setCountry($country['name']);
                    $location->setCountryFullname($country['fullname']);
                    $location->setIpMin($row[0]);
                    $location->setIpMax($row[1]);
                    if ($row[4] !== '-') {
                        $city = $this->getCity($row[4]);
                        $location->setCity($city['name']);
                        $location->setDistrict($city['district']);
                        $location->setRegion($city['region']);
                        $location->setLatitude($city['latitude']);
                        $location->setLongitude($city['longitude']);
                    }
                    $em->persist($location);
                    if ($i % 1000 == 0) {
                        $em->flush();
                        $em->clear();
                    }
                    $i++;
                    $progress->advance();
                }
                fclose($countriesHandler);
            }
            $em->flush();
            $progress->finish();
            $output->writeln('');
        }

        $output->writeln('Данные загружены.');

        return false;
    }

    /**
     * @param string $id
     * @return array|bool
     */
    private function getCity($id)
    {
        $kernel = $this->getContainer()->get('kernel');
        $citiesFile = $kernel->locateResource('@ADWGeoIpBundle/Bases/IpGeobase/cities.txt');
        if (file_exists($citiesFile)) {
            if ($citiesHandler = fopen($citiesFile, 'r')) {
                while (($row = fgetcsv($citiesHandler, null, "\t")) !== false) {
                    if ($row[0] == $id) {
                        return [
                            'name' => iconv("windows-1251", "utf8", $row[1]),
                            'district' => iconv("windows-1251", "utf8", $row[2]),
                            'region' => iconv("windows-1251", "utf8", $row[3]),
                            'latitude' => $row[4],
                            'longitude' => $row[5],
                        ];
                    }
                }
                fclose($citiesHandler);
            }
        }

        return false;
    }

    /**
     * @param string $code
     * @return string|bool
     */
    private function getCountryByIsoCode($code)
    {
        $kernel = $this->getContainer()->get('kernel');
        $isoFile = $kernel->locateResource('@ADWGeoIpBundle/Bases/IpGeobase/iso-3166-2.txt');
        if (file_exists($isoFile)) {
            if ($isoHandler = fopen($isoFile, 'r')) {
                while (($row = fgetcsv($isoHandler, null, "\t")) !== false) {
                    if ($row[3] == $code) {
                        return [
                            'name' => $row[0],
                            'fullname' => $row[1],
                        ];
                    }
                }
                fclose($isoHandler);
            }
        }

        return false;
    }
}