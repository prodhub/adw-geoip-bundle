<?php

namespace ADW\GeoIpBundle;

use ADW\GeoIpBundle\DependencyInjection\Compiler\GeoIpProvidersCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ADWGeoIpBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new GeoIpProvidersCompilerPass());
    }
}
